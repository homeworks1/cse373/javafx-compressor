package FileStream;

import Algo.HuffmanEncodedResult;

import java.io.*;

public class Tester {


    public static void main(String[] args) throws IOException, ClassNotFoundException {


        var in = new ObjectInputStream(new FileInputStream("compressed.compressed"));

        HuffmanEncodedResult result = (HuffmanEncodedResult) in.readObject();

        System.out.println(result);


    }
}
